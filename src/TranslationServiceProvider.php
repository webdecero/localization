<?php

namespace Webdecero\Localization;
 
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\TranslationServiceProvider as IlluminateTranslationServiceProvider;

class TranslationServiceProvider extends IlluminateTranslationServiceProvider
{
    
    
    
    private $translationFile = __DIR__ . '/../config/manager/translation-loader.php';
    
    /**
     * Register the application services.
     */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom($this->translationFile, 'manager.translation-loader');
    }

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
               $this->translationFile => config_path('manager/translation-loader.php'),
            ], 'config');

        }
    }

    /**
     * Register the translation line loader. This method registers a
     * `TranslationLoaderManager` instead of a simple `FileLoader` as the
     * applications `translation.loader` instance.
     */
    protected function registerLoader()
    {
        $this->app->singleton('translation.loader', function ($app) {
            $class = config('manager.translation-loader.translation_manager');

            return new $class($app['files'], $app['path.lang']);
        });
    }
}
