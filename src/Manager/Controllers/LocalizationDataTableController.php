<?php

namespace Webdecero\Localization\Manager\Controllers;

//Providers
//Models
//Helpers and Class
use Webdecero\Base\Manager\Controllers\DataTableController;

class LocalizationDataTableController extends DataTableController {

    protected $collectionName = 'localization';
    protected $searchable = [
        'query' => [
            '_id' => 'contains',
            'key' => 'contains',
            'text' => 'contains',
        ],
        'locale' => 'equal',
        'group' => 'equal',
    ];
    protected $fieldsDetails = [
        '_id' => 'ID',
        'locale' => 'Idioma',
        'group' => 'Grupo',
        'key' => 'Llave',
        'text' => 'Texto',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'ID',
        'locale' => 'Idioma',
        'group' => 'Grupo',
        'key' => 'Llave',
        'text' => 'Texto',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);


        if ($field == 'url_details') {

            $record = route('manager.localization.dataTable', ['id' => $item['_id']]);
        } else if ($field == 'opciones') {


            $record = "<a href=" . route('manager.localization.edit', ['id' => $item['_id'], 'type' => $item['type']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

            $record .= "<a href=" . route('manager.localization.destroy', ['id' => $item['_id']]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                    . "<i class='fa fa-trash-o'></i> "
                    . "</a>";
        }

        return $record;
    }

}
