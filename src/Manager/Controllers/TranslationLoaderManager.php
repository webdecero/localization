<?php

namespace Webdecero\Localization\Manager\Controllers;

use Illuminate\Translation\FileLoader;
use Webdecero\Localization\Manager\Controllers\TranslationLoader;

class TranslationLoaderManager extends FileLoader {

    /**
     * Load the messages for the given locale.
     *
     * @param string $locale
     * @param string $group
     * @param string $namespace
     *
     * @return array
     */
    public function load($locale, $group, $namespace = null) {
        $fileTranslations = parent::load($locale, $group, $namespace);
        
        
        if (!is_null($namespace) && $namespace !== '*') {
            return $fileTranslations;
        }

        $loaderTranslations = $this->getTranslationsForTranslationLoaders($locale, $group, $namespace);

        
        return array_replace_recursive($fileTranslations, $loaderTranslations);
    }

    protected function getTranslationsForTranslationLoaders(
    $locale, $group, $namespace = null
    ) {
        return collect(config('manager.translation-loader.translation_loaders'))
                        ->map(function ( $className) {
                            return app($className);
                        })
                        ->mapWithKeys(function (TranslationLoader $translationLoader) use ($locale, $group, $namespace) {
                            return $translationLoader->loadTranslations($locale, $group, $namespace);
                        })
                        ->toArray();
    }

}
