<?php

namespace Webdecero\Localization\Manager\Controllers;

//use Webdecero\Localization\Manager\Models\Localization;
//use Spatie\TranslationLoader\Exceptions\InvalidConfiguration;

class Db implements TranslationLoader
{
    public function loadTranslations( $locale,  $group)
    {
        $model = $this->getConfiguredModelClass();

        return $model::getTranslationsForGroup($locale, $group);
    }

    protected function getConfiguredModelClass()
    {
        $modelClass = config('manager.translation-loader.model');

//        if (! is_a(new $modelClass, Localization::class)) {
//            throw InvalidConfiguration::invalidModel($modelClass);
//        }

        return $modelClass;
    }
}
