<?php

namespace Webdecero\Localization\Manager\Controllers;

//Providers
use Validator;
use Auth;
//Models
use Webdecero\Localization\Manager\Models\Localization;
//Helpers and Class
use Illuminate\Http\Request;
use Webdecero\Base\Manager\Controllers\ManagerController;
use Webdecero\Base\Manager\Traits\DynamicInputs;
use Webdecero\Base\Manager\Facades\Utilities;

class LocalizationController extends ManagerController {

    use DynamicInputs;

    private $arrayLocaleNative = [];
    protected $configPages = '';

    public function __construct() {

        parent::__construct();

        $this->configPages = config('manager.localization');
        
        foreach (\LaravelLocalization::getSupportedLocales() as $key => $value) {
            $this->arrayLocaleNative[$key] = $value['native'];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $this->data['user'] = Auth::user();
		 $this->data['groups'] = Localization::groupBy('group')->get(['group'])->toArray();
  
                

                
                
        return view('baseViews::localizationPanel', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        

        $this->data['user'] = Auth::user();
        $this->data['dataPage'] = [];


        $type = request('type', 'index');
        $configPage = $this->configPages[$type];


        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;


        $processData = $this->processPageUI($configPage, $this->data);

        return view('baseViews::localizationAddForm', $processData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $input = $request->all();


        $rules = array(
            
            'key' => array('required'),
            'text' => array('required'),
            'group' => array('required')
            
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {


            $localization = new Localization;

            $this->_storeLocalization($request, $localization);



            return redirect()->route('manager.localization.index')->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
        $localization = Localization::findOrFail($id);

        $isValid = Utilities::idMongoValid($id);

        if (!$isValid || !isset($localization->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }



        $this->data['user'] = Auth::user();

        $this->data['localization'] = $localization;


        $this->data['dataPage'] = $localization->toArray();

        $type = request('type', 'index');
        $configPage = $this->configPages[$type];



        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;


        $processData = $this->processPageUI($configPage, $this->data);

        return view('baseViews::localizationEditForm', $processData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();


        $rules = array(
            'key' => array('required'),
            'text' => array('required'),
            'group' => array('required')
        );

        $input['id'] = $id;

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = Utilities::idMongoValid($input['id']);
            $localization = Localization::find($input['id']);

            if (!$isValid || !isset($localization->id)) {
                return redirect()->route('manager.localization.index')->with([
                            'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
                ]);
            }


            $this->_storeLocalization($request, $localization);


            return redirect()->route('manager.localization.index')->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $isValid = Utilities::idMongoValid($id);
        $localization = Localization::findOrFail($id);

        if (!$isValid || !isset($localization->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }


        $localization->delete();

        return redirect()->route('manager.localization.index')->with([
                    'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
        ]);
    }

    private function _storeLocalization(Request $request, $localization) {

        $input = $request->all();

        $localization->fill($input);

        $type = request('type', 'index');
        $sections = $this->configPages[$type]['sections'];


        $localization = $this->dynamicFillSave($localization, $sections, $input);



        $localization->save();

        return $localization;
    }

}
