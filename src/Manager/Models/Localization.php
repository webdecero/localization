<?php

namespace Webdecero\Localization\Manager\Models;

use Webdecero\Base\Manager\Models\Base;
use Illuminate\Support\Facades\Cache;

class Localization extends Base {

    /** @var array */
    public $translatable = ['text'];

    public function __construct() {


        $this->casts[] = ['text' => 'array'];
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'localization';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

//    public static function boot() {
//        
//        
//        parent::boot();
//    
//        static::saving(function (Localization $languageLine) {
//                \Debugbar::info($this);
//        dd($this);
//            
//            $languageLine->flushGroupCache();
//        });
//
//        static::saved(function (Localization $languageLine) {
//            
//                \Debugbar::info($this);
//        dd($this);
//            $languageLine->flushGroupCache();
//        });
//       
//    }

    public static function getTranslationsForGroup($locale, $group) {
        return Cache::rememberForever(static::getCacheKey($group, $locale), function () use ($group, $locale) {


                    $result = static::query()
                                    ->where('group', $group)
                                    ->where('locale', $locale)
                                    ->get()->reduce(function ($lines, Localization $languageLine) {
                        array_set($lines, $languageLine->key, $languageLine->text);

                        return $lines;
                    });


                    return (isset($result) && !empty($result)) ? $result : [];
                });
    }

    public static function getCacheKey($group, $locale) {
        return "webdecero.translation-loader.{$group}.{$locale}";
    }

//    /**
//     * @param string $locale
//     *
//     * @return string
//     */
//    public function getTranslation($locale) {
//
//
//        return isset($this->text) ? $this->text : $this->text;
//    }
//
//    /**
//     * @param string $locale
//     * @param string $value
//     *
//     * @return $this
//     */
//    public function setTranslation($locale, $value) {
//
//        $merge = (isset($this->text) && !empty($this->text)) ? $this->text : [];
//        $this->text = array_merge($merge, [$locale => $value]);
//
//        return $this;
//    }

    public function flushGroupCache() {

          Cache::forget(static::getCacheKey($this->group, $this->locale));
    }

}
