@extends('baseViews::layouts.dataTable')


@section('sectionAction', 'Tabla')
@section('sectionName', 'Localization')
@section('formAction', route('manager.localization.dataTable'))


@section('inputs')

<div class="form-group">
    <label for="locale">Idioma</label> <br>

    <select class="reloadTable" name="locale">
        <option value="">Ninguno</option>
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        <option value="{{ $localeCode }}">{{ $properties['native'] }}</option>
        @endforeach


    </select>
</div>


<div class="form-group">
    <label for="group">Grupos</label> <br>

    <select class="reloadTable" name="group">
        <option value="">Ninguno</option>
        @foreach($groups as $group)
        <option value="{{ $group['group'] }}">{{ $group['group']  }}</option>
        @endforeach


    </select>
</div>



<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

    <div class="btn-group" role="group" aria-label="...">

        <a class="btn btn-success" href="{{ route('manager.localization.create',['type'=> 'index'])}}" >
            <i class="fa fa-plus-circle"></i> Nuevo Elemento de tradución
        </a>
        
         
    </div>
    <div class="btn-group" role="group" aria-label="...">
        <button type="submit"class="btn btn-success " >

            <i class="fa fa-file-excel-o"></i> EXPORT EXCEL

        </button>
    </div>
</div>

@endsection


@section('dataTableColums')


<th data-details="true" ></th>


<th data-name="locale" data-orderable="true" >
    <strong>Idioma</strong>
</th>


<th data-name="group" data-orderable="true">
    <strong>Grupo</strong>
</th>
<th data-name="key" data-orderable="true" >
    <strong>Llave</strong>
</th>


<th data-name="text"  >
    <strong>Texto</strong>
</th>


<th data-name="created_at"><strong>Fecha Alta</strong></th>

<th data-name="opciones">
    <strong>Opciones</strong>
</th>




@endsection
