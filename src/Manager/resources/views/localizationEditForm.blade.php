@extends($layout)


@section('sectionAction', 'Editar')
@section('sectionName', $sectionName)
@section('formAction', route('manager.localization.update',[$localization->id]))

@section('inputs')
{{ method_field('PUT') }}

{!! $htmlInputs !!}

@endsection





