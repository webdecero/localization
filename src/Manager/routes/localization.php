<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//localization
Route::post('localization/dataTable/{id?}/{excel?}', array('as' => 'manager.localization.dataTable', 'uses' => 'LocalizationDataTableController@dataTable'));
Route::get('localization/{id}/delete', ['as' => 'manager.localization.destroy', 'uses' => 'LocalizationController@destroy']);
Route::resource('localization', 'LocalizationController', [
    'except' => [
        'show', 'destroy'
    ],
    'names' => [
        'index' => 'manager.localization.index',
        'create' => 'manager.localization.create',
        'store' => 'manager.localization.store',
        'show' => 'manager.localization.show',
        'edit' => 'manager.localization.edit',
        'update' => 'manager.localization.update',
//        'destroy' => 'manager.localization.destroy',
    ]
]);

