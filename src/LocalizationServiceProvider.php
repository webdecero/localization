<?php

namespace Webdecero\Localization;

use Illuminate\Support\ServiceProvider;
use RuntimeException;
use Illuminate\Support\Facades\Route;


class LocalizationServiceProvider extends ServiceProvider {

    
    
    private $configLocalization = __DIR__ . '/../config/manager/localization.php';
    
    private $folderViews = __DIR__ . '/Manager/resources/views';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

       
//        Publishes Configuración Base
//        php artisan vendor:publish --provider="Webdecero\Localization\LocalizationServiceProvider" --tag=config

        $this->publishes([
            $this->configLocalization => config_path('manager/localization.php'),
            
                ], 'config');

//        Registra Views
        $this->loadViewsFrom($this->folderViews, 'baseViews');

//ROUTE Localization 
        $config = $this->app['config']->get('manager.base.manager.route', []);

        if (empty($config)) {
            throw new RuntimeException('No se enecontro la configuración Base para ruta');
        }

        $config['namespace'] = 'Webdecero\Localization\Manager\Controllers';
        
        $originalMiddleware =$config['middleware'];

 
        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/localization.php');
        });
        

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {


        
        
        $this->mergeConfigFrom($this->configLocalization, 'manager.localization');
        
        
        

    }

}
